import React from 'react'
import {GET_ALL_USERS} from '../../Graphql/Queries'
import {useQuery, useMutation} from '@apollo/client'
import {DELETE_USER} from '../../Graphql/Mutation'


function ListOfUsers() {


    // Create a hook and destructuring it and the GET_ALL_USERS query
    const {data,  loading} = useQuery(GET_ALL_USERS)
    const [deleteUser, {error}] = useMutation(DELETE_USER)

    if (loading) {
        return <p>Loading...</p>;
    }else {
        if (error) return <p>Error :(</p>;
    }
  
    


    return (
        <div>
           {data &&
               data.getAllUsers.map((user: any, id) => {
               return (
                  <div key={id}>
                      <p>Name:  {user.name} {user.username}</p>
                            <button onClick={() => {
                                deleteUser({ variables: { id: user.id } });
                            }}>
                           Delete User
                       </button>
                  </div>
               )
           })}
        </div>
    )
}

export default ListOfUsers
