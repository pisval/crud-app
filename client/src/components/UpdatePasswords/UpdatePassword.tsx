import React, { useState} from 'react'
import {UPDATE_PASSWORD} from '../../Graphql/Mutation'
import {useMutation} from '@apollo/client'



function UpdatePassword() {

    const [username, setUsername] = useState(" ")
    const [oldPassword, setOldPassword] = useState(" ")
    const [newPassword, setNewPassword] = useState(" ")



    // Use the mutation hook here
    const [updatePassword, {error}] = useMutation(UPDATE_PASSWORD)
    
    if(error){
        return <h6>{error}</h6>
    }



    return (
        <div>
            <h2>Update Password</h2>
            <form>
                <input 
                    type="text" 
                    placeholder="Username..."
                    onChange={(event) => {
                        setUsername(event.target.value)
                    }}
                    
                />
                <input 
                    type="password" 
                    placeholder="Old Password..."
                    onChange={(event) => {
                        setOldPassword(event.target.value);
                    }}
                />
                <input 
                   type="password" 
                   placeholder="New Password..."
                   onChange={(event) => {
                    setNewPassword(event.target.value);
                   }}
                />

                <button className="btn btn-primary" onClick={() => {
                    updatePassword(
                        {variables: {
                            username: username, 
                            oldPassword: oldPassword, 
                            newPassword: newPassword
                        }})
                }}>Update Password</button>
            </form>
        </div>
    )
}

export default UpdatePassword
