import React, { useState, useEffect } from 'react'
import {CREATE_USER} from '../../Graphql/Mutation'
import { useMutation } from "@apollo/client";


function CreateUser() {
    const [name, setName] = useState("");
    const [userName, setUserName] = useState("");
    const [password, setPassword] = useState("");
    // Create useMutation hooks
    const [createUser, { error }] = useMutation(CREATE_USER);
    const [inputValue, setInputValue] = useState("");


     // Input Field handler
      const handleUserInput = (e) => {
        setInputValue(e.target.value);
      };


        // Reset Input Field handler
        const resetInputField = () => {
          setInputValue("");
        };





    //Create A simple form
    return (
      <>
         <h2>Create New User</h2>
         <form>
         <input type="text" value={inputValue} onChange={handleUserInput} />
         <button onClick={resetInputField}>Reset</button>

        <div className="createUser">
              <input
                
                type="text"
                placeholder="name"
                onChange={(event) => {
                  setName(event.target.value);
                }}
              />
              <input
                
                type="text"
                placeholder="username"
                onChange={(event) => {
                  setUserName(event.target.value);
                }}
              />
              <input
                
                type="text"
                placeholder="password"
                onChange={(event) => {
                  setPassword(event.target.value);
                }}
              />
              <button type="reset" className="btn btn-primary"
                onClick={() => {
                  createUser({
                    variables: {
                      name: name,
                      username: userName,
                      password: password,
                    },
                  });
                }}
              >
            Create User
          </button>
       </div>
    </form>
    </>
    )
}

export default CreateUser
