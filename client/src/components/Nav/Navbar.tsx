import React from 'react'

function Navbar() {
return (
<div className="container">
    <div className="row col-lg-col-8">
        <div className="col">
            <img src="#" alt="cDesign" />
        </div>
    </div>

    <div className="row text-end  mb-0">
        <div className="col">
        <ul className="list-inline">
            <ul className="list-inline-item"><a href="/">Home</a></ul>
            <ul className="list-inline-item"><a href="/about">Contact</a></ul>
            <ul className="list-inline-item"><a href="#">Portfolio</a></ul>
            <ul className="list-inline-item"><a href="#">Blog</a></ul>
            <ul className="list-inline-item"><a href="#">Login</a></ul>
        </ul>
        </div>
    </div>
</div>
)
}

export default Navbar