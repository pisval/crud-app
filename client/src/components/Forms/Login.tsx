import React from 'react'
import {GET_ALL_USERS} from '../../Graphql/Queries'
import {useQuery, useMutation} from '@apollo/client'
import {DELETE_USER} from '../../Graphql/Mutation'
import '../Forms/Style.css'


const Login = () => {
const {data, loading} = useQuery(GET_ALL_USERS)
const [deleteUser, {error}] = useMutation(DELETE_USER)


if (loading) {
return <p>Loading...</p>;
}else {
if (error) return <p>Error :(</p>;
}




return (
<>
    <div className="wrapper">
        <div className="container">
            <div className="row ">
                <div className="col-sm-4 offset-sm-4">

                    <form className="mt-3 card p-3 forms">
                        <div className="row mb-3 ">
                            <div className="col text-center">
                                <img src="images/user.png" className="brand_logo" alt="Logo" />
                            </div>
                        </div>
                        <div className="mb-3">
                            <input type="text" name="" className="form-control" placeholder="Username" />
                        </div>

                        <div className="mb-3">
                            <input className="form-check-input" type="checkbox" value="" />
                            <label className="form-label form-text">Remember me</label>
                        </div>

                        <div className="mb-3">
                            <input type="password" name="" className="form-control" placeholder="password" />
                        </div>
                        <div>
                            <input type="submit" className="form-submit btn btn-primary" placeholder="Login" />
                        </div>

                        <div className=" form-text mt-4 text-center">
                            Don't have an account? <a href="/" className="ml-2">Sign Up</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</>
)
}

export default Login