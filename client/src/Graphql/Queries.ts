import { gql } from "@apollo/client";


// GET_ALL_USERS mutation Query for getting all the users data
export const GET_ALL_USERS = gql`
query getAllUsers { 
    getAllUsers {
        id
        name
        username
    }
}
 
`;

/**
 * Note: The (getAllUsers) query is the query
 *  that comes to the query in the backend 
 *  within the schema in the file named index.ts s
 */