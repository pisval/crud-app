import { gql } from "@apollo/client";


// CREATE_USER mutation
export const CREATE_USER = gql`
  mutation createUser(
      $name: String!,
      $username: String!, 
      $password: String!) {
    createUser(
        name: $name, 
        username: $username, 
        password: $password
    ) {
      id
      name
      username
    }
  }
`;


// DELETE_USERS mutation
export const DELETE_USER = gql`
  mutation deleteUser($id: ID!) {
    deleteUser(id: $id) {
      successful
    }
  }
`;


export const UPDATE_PASSWORD = gql`
  mutation updateUser(
    $username: String!
    $oldPassword: String!
    $newPassword: String!
  ) {
    updateUser(
          username: $username
          oldPassword: $oldPassword
          newPassword: $newPassword
    ) {
      message
    }
  }
`;


