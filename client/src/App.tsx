import React, { useEffect, useState } from 'react';
import './App.css';
//import apollo dependencies
import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";
import CreateUser from './components/CreateUser/CreateUser';
import ListOfUsers from './components/DisplayUsersList/ListOfUsers';
import UpdatePassword from './components/UpdatePasswords/UpdatePassword';
import { BrowserRouter as Router,Switch, Route } from "react-router-dom";
import Login from './components/Forms/Login';
import Footer from './components/Footers/Footer';
import Navbar from './components/Nav/Navbar';





function App() {

  

  //Create a client object
  const client = new ApolloClient({
    uri: "http://localhost:3001/graphql",
    cache: new InMemoryCache(),
  });

  //console.log(client);



  return (
    <>
    <Navbar /> 
    <Router>
        <ApolloProvider client={client}>
            <Switch> 
               <Route exact path="/">
                  <Login />
               </Route>

               <Route path="/about">
                  <UpdatePassword />
               </Route>
            </Switch>
        </ApolloProvider>
    </Router>
    <Footer />
  </>
  );
}

export default App;
