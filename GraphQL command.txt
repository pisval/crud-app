# Keyboard shortcuts:
#
#     Prettify Query:  Shift-Ctrl-P (or press the prettify button above)
#
#     Merge Query:  Shift-Ctrl-M (or press the merge button above)
#
#     Run Query:  Ctrl-Enter (or press the play button above)
#
#     Auto Complete:  Ctrl-Space (or just start typing)



#Command to run a query in GraphQL in the browser
query {
  getAllUsers {
    name
    username
    password
  }
}



# Command to Insert data into a table using GRAPHQL 
mutation {
  createUser(name:"pinto" , username: "pintotech" , password: "rj200100p" ){
    name,
    username,
    password
  }
}



//Command to UPDATE  data into a table using GRAPHQL
mutation {
  updateUser(username: "Manuel" , oldPassword: "rj200100p", newPassword: "p@ssw0rd" ,){
      username,
  }
}


//Command to DELETE data into a table using GRAPHQL
mutation {
  deleteUser(id: 3){
    id
  }
}

///////////////////////////////////////////////////
mutation {
  deleteUser(id: 11){
    successful
    message
  }
}