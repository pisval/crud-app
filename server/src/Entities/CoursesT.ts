import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from 'typeorm';


@Entity()
export class Courses extends BaseEntity {
    // Table colum
    // passing the Id to a table
    @PrimaryGeneratedColumn()
    courseId!: number;
    @Column()
    courseName!: string;
    @Column()
    courseDuration!: string;
    @Column()
    courseLecture!: string;
    @Column()
    courseClass!: string; 
    @Column()
    courseYears!: number;
    @Column()
    courseEndYear!: number;
}
