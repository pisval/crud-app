import {Entity, BaseEntity , PrimaryGeneratedColumn, Column } from 'typeorm';


@Entity()
export class Carousel extends BaseEntity {

    @PrimaryGeneratedColumn()
    image_id!: number;
    @Column()
    image!: string;
}


