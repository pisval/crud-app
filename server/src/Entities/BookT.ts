import {BaseEntity, Entity, Column, PrimaryGeneratedColumn } from 'typeorm'


@Entity()
export class Books extends BaseEntity {
    //Table Column

    //Book primary key
    @PrimaryGeneratedColumn()
    book_id!: number;
    @Column()
    book_name!: string;
    @Column()
    book_author!: string;
    @Column()
    book_year!: number;
    @Column()
    book_title!: string;
    @Column()
    book_published!: string;
    @Column()
    image!: string;
    @Column()
    book_isAvailable!: boolean;
    @Column()
    book_isNotAvailable!: boolean;
}