//Users table
import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";


//Declarator
@Entity()
export class Users extends BaseEntity {
   // table columns

   // passing the Id of table
   @PrimaryGeneratedColumn()
   id!: number; 
   @Column()
   name!: string;
   @Column()
   username!: string;
   @Column()
   password!: string;
   
}
