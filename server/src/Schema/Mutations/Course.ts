import {GraphQLID, GraphQLString, GraphQLInt} from 'graphql';
import { resolve } from 'path/posix';
import { Courses } from '../../Entities/CoursesT';
import {CourseType} from '../TypeDefs/Course';
import {MessageType}  from '../TypeDefs/Messages';



// Get all Course mutation
export const CREATE_COURSE = {
    type: CourseType,
    args: {
        courseName: {type: GraphQLString },
        courseDuration: {type: GraphQLString },
        courseLecture: {type: GraphQLString },
        courseClass: {type: GraphQLString },
        courseYears: {type: GraphQLInt},
        courseEndYear: {type: GraphQLInt},
    },

    async resolve(parent: any, args: any) {
        const {
            courseName, 
            courseDuration, 
            courseLecture, 
            courseClass, 
            courseYears, 
            courseEndYear 
        } = args;

        // Insert data into DB table query
        await Courses.insert({
            courseName, 
            courseDuration, 
            courseLecture, 
            courseClass, 
            courseYears, 
            courseEndYear
        })

        return args
    },
}


// Update course mutation
export const UPDATE_COURSE = {
    type: MessageType,
    args: {
        courseName: {type: GraphQLString},
        courseDuration: {type: GraphQLString},
        courseLecture: {type: GraphQLString},
        courseClass: {type: GraphQLString},
        courseYears: {type: GraphQLString},
        courseEndYear: {type: GraphQLString}
    },

    async resolve(parent: any , args: any){
        const {
            courseName, 
            courseDuration,
            courseLecture, 
            courseClass,
            courseYears,
            courseEndYear
        } = args

        const course = await Courses.findOne({courseName: courseName}) 

        // Error handling to check if the user exists
        if(!course){
            throw new Error("COURSE NAME DOES NOT EXISTS")
        }else{
            
        }
    }
}