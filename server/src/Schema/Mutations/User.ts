import { GraphQLID, GraphQLString } from "graphql";
import { UserType } from "../TypeDefs/User";
import {Users} from '../../Entities/UsersT';
import {MessageType} from "../TypeDefs/Messages";

// Create a new User Table
export const CREATE_USER = {
    type: UserType, // add the message type definition
    args: {
        name: {type: GraphQLString },
        username: {type: GraphQLString },
        password: {type: GraphQLString }
    },

    async resolve(parent: any, args: any){
       const {name, username, password} = args;

       //Insert data into the  DB table query. Note I could only pass the {args}
       await Users.insert({ name, username, password })

       return args
    },
}



//Update mutation
export const UPDATE_PASSWORD = {
    type: MessageType, // add the message type definition
    args: {
        username: {type: GraphQLString },
        oldPassword: {type: GraphQLString},
        newPassword: {type: GraphQLString}
    },

    async resolve(parent: any, args: any){
       const { username, oldPassword, newPassword } = args
       const user = await Users.findOne({username: username})

       //Error handling check the user if exists
       if (!user) {
           throw new Error("USERNAME DOES NOT EXISTS");
       }
       const userPassword = user?.password

       if (oldPassword === userPassword) {
           await Users.update(
              { username: username},
              { password: newPassword }
            );

            //Another error checking
            return { successful: true, message: "PASSWORD UPDATED"}
       }else {
           throw new Error("PASSWORD DO NOT MATCH!");
       }
    },
}




// Delete mutation
export const DELETE_USER = {
    type: MessageType, // add the message type definition
    args: {
        id: {type: GraphQLID },
    },

    async resolve(parent: any, args: any){
       const {id} = args;
       
       //Add type form to delete.
       await Users.delete(id) 

       if(Users !=id){
           throw new Error("USERNAME WITH SPECIFIED ID DOES NOT EXIST")
       }else {
           // returning a message
           return {successful: true, message: "DELETE WORKED"}  
       }
       //Note: To delete specific username use this code (await TbName.delete({username: "Pinto"}))
    },
}   



