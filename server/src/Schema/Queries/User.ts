import {GraphQLList} from 'graphql';
import { UserType } from '../TypeDefs/User';
import {Users} from "../../Entities/UsersT"


export const GET_ALL_USERS = {
    type: new GraphQLList(UserType),

    resolve(){ // If I want to create an interface add this code within resolve ":Promise<IUser[]> "
        //Executing the Query
        return Users.find();
    }
}