import {GraphQLObjectType, GraphQLID, GraphQLString, GraphQLInt, GraphQLBoolean } from 'graphql';


export const BookType = new GraphQLObjectType({
    name: "Book",
    fields: () => ({
        book_id: { type: GraphQLID },
        book_name: {type: GraphQLString},
        book_author: {type: GraphQLString},
        book_year: {type: GraphQLInt},
        book_title: {type: GraphQLString},
        book_published: {type: GraphQLString},
        image: {type: GraphQLString},
        book_isAvailable: {type: GraphQLBoolean},
        book_isNotAvailable: {type: GraphQLBoolean}
    })
})