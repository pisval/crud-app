import {GraphQLObjectType, GraphQLID, GraphQLString, GraphQLInt } from 'graphql';


export const CourseType = new GraphQLObjectType({
    name: "Course",
    fields: () => ({
        courseId: {type: GraphQLID},
        courseName: {type: GraphQLString},
        courseDuration: {type: GraphQLString},
        courseLecture: {type: GraphQLString},
        courseClass: {type: GraphQLString},
        courseYears: {type: GraphQLInt},
        courseEndYear: {type: GraphQLInt}
    })
})