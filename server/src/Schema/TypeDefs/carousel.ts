import {GraphQLObjectType, GraphQLString, GraphQLID, GraphQLBoolean, graphql,} from 'graphql'


export const CarouselType = new GraphQLObjectType({
    name: "Carousel",
    fields: () => ({
        image_id: { type: GraphQLID},
        image: { type: GraphQLString}
    })
})