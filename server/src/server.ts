import express from 'express';
import {graphqlHTTP} from 'express-graphql';
import cors from 'cors';
import {createConnection} from 'typeorm';
import {schema} from '../src/Schema';
import { Users } from '../src/Entities/UsersT'

const main = async () => {

   //  Create mysql connection
   await createConnection({
       type: "mysql",
       port: 3308,
       database: "cruddb",
       username: "root",
       password: "",
       logging: true,
       /**
        * Whenever I want to create a new 
        * table I just need to set the synchronize to true
        * followed by the table entity name
        */
       synchronize: false,
       entities: [Users],
   }) // End mysql connection

  
   
   // create an express app const variable
   const app = express()

   //Add cors to connect the frontend app to the backend app
   app.use(cors())

   // Use a express middleware
   app.use(express.json())

    //Graphql middleware and let comment it just for now 
     app.use("/graphql",graphqlHTTP({
          schema, 
          graphiql: true
    }))

    app.listen(3001, () => {
        console.log("SERVER RUNNING ON PORT 3001")
    })
};

main().catch((err) => {
   console.log(err)
});
